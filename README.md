# CERN-IPMC Firmware

The AdvancedTCA standard has been selected as one of the hardware platforms
for the upgrades of the back-end electronics of the CMS and ATLAS experiments of the Large Hadron
Collider. In this context, the CERN EP-ESE group has adapted a commercial IPMC solution from [Pigeon Point](http://www.pigeonpoint.com/pdf/BMR-A2F-ATCA-pb.pdf)
and designed a mezzanine card for the management of AdvancedTCA blades.

This repository contains the module's source code as well as additional tools used to generate
the CERN-IPMC binary customized to fit the user specification. This page describes how to customize the CERN-IPMC
software to fit the user's requirements.

# Outline:

* [Repository architecture](#repository-architecture)
* [Dependencies](#dependencies)
    * [Python libraries](#python-libraries)
    * [Ipmitool](#ipmitool)
* [How to](#how-to)
    * [Project creation, compilation and module upgrade](#project-creation-compilation-and-module-upgrade)
    * [Adding user functions and commands](#adding-user-functions-and-commands)
    * [Creation of the XML configuration file](#xml-configuration-file)
        * [General configuration](#general-configuration)
        * [LAN configuration](#lan-configuration)
        * [AMC configuration](#amc-slots)
        * [i-RTM configuration](#i-rtm-slot)
        * [Power configuration](#power-sequences)
        * [Sensors configuration](#sensors) 
* [Foreseen](#foreseen)

# Repository architecture

The repository is mainly made of the 3 following parts:

-   **ipmc-sensors**: The ipmc-sensors folder contains sensor drivers ready to be used
-   **ipmc-user**: The ipmc-user folder contains user C files where additional features and/or OEM commands can be defined.
-   **compile.py**: Python script implementing the compilation mechanism

# Dependencies

## Python libraries

The compile.py python script requires the following dependencies:

- <b>colorama</b> (installation command: pip install colorama)
- <b>lxml</b> (installation command: pip install lxml)
- <b>requests</b> (installation command: pip install requests)

## Ipmitool

Ipmitool can be used to upgrade the CERN-IPMC module using HPM.1.

> ### How to install ipmitool on Windows (Cygwin)
>
> 1. Download Cygwin ([link](https://cygwin.com/setup-x86.exe))
> 2. Select the following packages during the installation:
>   -   gcc-core (Devel category)
>   -   make (Devel category)
>   -   openssl-devel
> 3. Download ipmitool sources ([link](http://sourceforge.net/projects/ipmitool/files/ipmitool/))
> 4. Extract the archive
> 5. Open cygwin and go to the directory
> 6. Run "./configure --enable-intf -lanplus"
> 7. Run "make"
> 8. Ipmitool executable can be now executed via Cygwin : "src/ipmitool.exe"

# How to

This section describes how to customize, compile and upgrade the CERN-IPMC. It provides a detailed documentation on how to edit the 
configuration file used to adapt the CERN-IPMC software according to the requirements: FRU information, AMC/iRTM support, sensors, etc.
This action was simplified as much as possible and can be automatically made using an XML file and a python script.

## Project creation, compilation and module upgrade

> ### Clone and folder architecture creation
> 1. Clone the git repository (git clone https://gitlab.cern.ch/ep-ese-be-xtca/ipmc-project.git)
> 
> ### Customize the module
> 2. Edit the config.xml file (detailed in "Creation of the configuration XML file")
> 3. Add your sensor drivers (detailed <a href='https://espace.cern.ch/ph-dep-ESE-BE-ATCAEvaluationProject/PP_IPMC/Public%20documents/CERN-IPMC%20-%20Sensor%20driver%20design%20guide%20.pdf'>here</a>)
> 4. Add user commands / functions (detailed in "Adding user functions and commands")
>
> ### Compile the project
> 5. Run the compile.py script (cmd: python compile.py)
>
> ### Upgrade the ipmc using ipmitool
> 6. Using direct ethernet connection to the IPMC:
>   -   Execute "./ipmitool -I lan -H &#x3C;ipmc_ip&#x3E; -U "" -P "" hpm upgrade hpm1all.img force
>   -   Execute "./ipmitool -I lan -H &#x3C;ipmc_ip&#x3E; -U "" -P "" hpm activate
>
> **OR**
>
> 13. Using ethernet connection to the shelf manager:
>   -   Execute "./ipmitool -I lan -H &#x3C;shmm_ip&#x3E; -U "" -P "" -t &#x3C;ipmc_ipmb_addr&#x3E; hpm upgrade hpm1all.img force
>   -   Execute "./ipmitool -I lan -H &#x3C;shmm_ip&#x3E; -U "" -P "" -t &#x3C;ipmc_ipmb_addr&#x3E; hpm activate

**Note**: The python script requieres the following package to be executed (dependency): colorama, lxml.

## Adding user functions and commands

User function and OEM commands can be easily added by creating/editing a file located into the ipmc-user directory.
A few commented example are already available and show how to implement callbacks called depending on:

- main loop iteration
- timer

## XML configuration file

The base configuration of the CERN-IPMC can be made using an XML file. An example, named config-ex.xml, is available in the repository.
This section describes how to modify it or create a new one.

### File template

```xml
<?xml version="1.0" encoding="UTF-8"?>

<IPMC>
    <!-- Configuration nodes -->
</IPMC>
```

### General configuration

This node is mandatory and used to generate the FRU information as well as the config_atca.h header file.

```xml
<GeneralConfig>

	<DeviceID>0x12</DeviceID>                           <!-- Device ID (decimal value) -->
	<DeviceRevision>0x00</DeviceRevision>               <!-- Device revision (decimal value) -->
	<ManufacturerID>0x000060</ManufacturerID>           <!-- Manufacture ID (decimal value, CERN: 0x000060)                 -->
	                                                    <!--    - IANA: http://www.iana.org/assignments/enterprise-numbers  -->
	<ProductID>0x1236</ProductID>                       <!-- Product ID (decimal value) -->
	
	<ManufacturingDate>06/01/2017</ManufacturingDate>   <!-- Manufacturing date, must be MM/DD/YYYY -->
	
	<BoardManuf>Cirly/Addax</BoardManuf>                <!-- Board manufacturer (string) -->
	<BoardName>IPMC-TestPAD board</BoardName>           <!-- Board name (string) -->
	<BoardSN>00001</BoardSN>                            <!-- Board serial number (string) -->
	<BoardPN>P580050995</BoardPN>                       <!-- Board part number (string) -->
	
	<ProductManuf>CERN</ProductManuf>                   <!-- Product manufacturer (string) -->
	<ProductName>IPMC-TestPAD</ProductName>             <!-- Product name (string) -->
	<ProductPN>PN00001</ProductPN>                      <!-- Product part number (string) -->
	<ProductSN>0000001</ProductSN>                      <!-- Product serial number (string) -->
	<ProductVersion type="major">1</ProductVersion>	    <!-- Product version major value (decimal) -->
	<ProductVersion type="minor">20</ProductVersion>	<!-- Product version minor value (decimal) -->
	
	<MaxCurrent>60.0</MaxCurrent>                       <!-- ATCA blade maximum current (including the one required for the AMCs) -->
	<MaxInternalCurrent>1.0</MaxInternalCurrent>        <!-- Maximum current required by the ATCA blade (excluding the AMCs) -->
	
	<!-- <ResetOnWrongHAEn /> -->                       <!-- Enable the Reset on Wrong HA feature -->
    <!-- <PowerMonitoringEn /> -->                      <!-- Enable power monitoring based on PowerGood A/B signals -->
    <!-- <AlertMonitoringEn /> -->                      <!-- Enable the alert input monitoring -->
        
	<!-- Hardware -->
	<HandleSwitch active="LOW" inactive="HIGH" />       <!-- Active/De-active state of the handle switch -->

    <SerialIntf>SDI_INTF</SerialIntf>                   <!-- Select the interface routed to the DIMM connector: -->
                                                        <!--    - Serial Over Lan: SOL_INTF                     -->
                                                        <!--    - Serial Debug Intf: SDI_INTF                   -->
    <RedirectSDItoSOL/>                                 <!-- Redirect the Serial Interface to the SOL interface (Ethernet)      -->
                                                        <!--    - Note: can be used only when SerialIntf = SDI_INTF             -->
                                                        <!--    - Note: Remove this node to disable the redirection             -->
    
</GeneralConfig>
```

### LAN configuration

This node is mandatory and is used to generate the config_lan.h header file. Only the "default" input in the 
IPAddrList is mandatory, all of the others are optional and are used to define the IP address of the IPMC
depending on the slot position.

```xml
<LANConfig>	
	
	<MACAddr>0A:0A:0A:0A:0A:86</MACAddr>                    <!-- MAC address of the Ethernet Phy. -->
	<NetMask>255.255.255.0</NetMask>                        <!-- Network netmask -->
	<GatewayIP>192.138.1.3</GatewayIP>                      <!-- Network gateway IP address -->

    <!-- <EnableDHCP /> -->                                 <!-- Enabe DHCP feature -->
    
	<IPAddrList>                                            <!-- List of IP address (only used when DHCP is not active) -->
		<IPAddr slot_addr="default">192.168.1.34</IPAddr>   <!-- Default IP address when specific one for the slot is not define -->
		<IPAddr slot_addr="0x41">192.168.1.20</IPAddr>      <!-- IP address for slot 1 (0x41) -->
		<IPAddr slot_addr="0x42">192.168.1.21</IPAddr>      <!-- ... -->
		<IPAddr slot_addr="0x43">192.168.1.22</IPAddr>      <!-- ... -->
		<IPAddr slot_addr="0x44">192.168.1.23</IPAddr>      <!-- ... -->
		<IPAddr slot_addr="0x45">192.168.1.24</IPAddr>      <!-- ... -->
		<IPAddr slot_addr="0x46">192.168.1.25</IPAddr>      <!-- ... -->
		<IPAddr slot_addr="0x47">192.168.1.26</IPAddr>      <!-- ... -->
		<IPAddr slot_addr="0x48">192.168.1.27</IPAddr>      <!-- ... -->
		<IPAddr slot_addr="0x49">192.168.1.28</IPAddr>      <!-- ... -->
		<IPAddr slot_addr="0x4a">192.168.1.29</IPAddr>      <!-- ... -->
		<IPAddr slot_addr="0x4b">192.168.1.30</IPAddr>      <!-- ... -->
		<IPAddr slot_addr="0x4c">192.168.1.31</IPAddr>      <!-- ... -->
		<IPAddr slot_addr="0x4d">192.168.1.32</IPAddr>      <!-- ... -->
		<IPAddr slot_addr="0x4e">192.168.1.33</IPAddr>      <!-- ... -->
		<IPAddr slot_addr="0x4f">192.168.1.34</IPAddr>      <!-- ... -->
		<IPAddr slot_addr="0x50">192.168.1.35</IPAddr>      <!-- IP address for slot 16 (0x50) -->
	</IPAddrList>
	
</LANConfig>
```

### AMC Slots

This node must be define when the CERN-IPMC module is used on a blade supporting AMCs (AdvancedTCA Carrier blade). 
It contains the configuration of the AMC slots and generates the config_amc.h header file. The
AMCSlots node should contain as many AMC node as the number of AMC slots available on the carrier.

```xml
<AMCSlots>

	<AMC site="1">                                  <!-- AMC slot 1 (IPMB-L addr: 0x72) -->
		<PhysicalPort>1</PhysicalPort>              <!-- Physical port of the CERN-IPMC mezzanine card (from 0 to 8)-->
		<MaxCurrent>6.0</MaxCurrent>                <!-- Maximum current supported for the AMC slot -->
		<PowerGoodTimeout>300</PowerGoodTimeout>    <!-- Timeout for Management and Payload power -->
		<DCDCEfficiency>85</DCDCEfficiency>         <!-- Efficiency of the power switch / DC/DC -->
	</AMC>

	<!-- Additional slots -->
	
</AMCSlots>
```

### i-RTM Slot

This node must be define only when the CERN-IPMC module is used on a blade supporting i-RTM support. 
It contains the configuration of the i-RTM and generates the config_rtm.h header file.

```xml
<iRTMSlot>
	<PhysicalPort>0</PhysicalPort>              <!-- Physical port of the CERN-IPMC mezzanine card (from 0 to 8)-->
	<MaxCurrent>6.0</MaxCurrent>                <!-- Maximum current supported for the i-RTM slot -->
	<Address>0xea</Address>                     <!-- Address of the i-RTM on the IPMB-L bus -->
	<PowerGoodTimeout>300</PowerGoodTimeout>    <!-- Timeout for Management and Payload power -->
	<DCDCEfficiency>85</DCDCEfficiency>         <!-- Efficiency of the power switch / DC/DC -->
</iRTMSlot>
```

### Power sequences

This node defines the power ON and OFF sequences used to generates the config_power.h header file. 

```xml
<PowerManagement>

	<PowerONSeq>                                                    <!-- Power ON sequence -->
	    <step>PSQ_ENABLE_SIGNAL(CFG_PAYLOAD_DCDC_EN_SIGNAL)</step>  <!-- Active the 12V Enable pin -->
	    <step>...</step>                                            <!-- Can be one of the step listed below -->
		<step>PSQ_END</step>                                        <!-- End of the sequence -->
	</PowerONSeq>
	
	<PowerOFFSeq>                                                   <!-- Power OFF sequence -->
		<step>PSQ_DISABLE_SIGNAL(CFG_PAYLOAD_DCDC_EN_SIGNAL)</step> <!-- Active the 12V Enable pin -->
	    <step>...</step>                                            <!-- Can be one of the step listed below -->
		<step>PSQ_END</step>                                        <!-- End of the sequence -->
	</PowerOFFSeq>
	
</PowerManagement>
```

The steps can be:

- **PSQ_ENABLE_SIGNAL(signal_name)**: Activate a GPIO signal.
- **PSQ_DISABLE_SIGNAL(signal_name)**: De-activate a GPIO signal.
- **PSQ_TEST_SIGNAL_JUMP_IF_SET(signal_name, offset)**: Jump offset instruction (positive or negative) when the signal is activated. Otherwise, continue to the next step.
- **PSQ_TEST_SIGNAL_JUMP_IFNOT_SET(signal_name, offset)**: Same as previous instruction, but check if signal is de-activated.
- **PSQ_SET_TIMER(timer, timeout)**: Set a timer (0 - based) to a given timeout value (in milliseconds).
- **PSQ_JUMP(offset)**: Jump offset steps (positive or negative).
- **PSQ_JUMP_IF_TIMEOUT(timer, offset)**: Jump offset instruction (positive or negative) if the specified timer has expired. Otherwise, continue to the next step.
- **PSQ_JUMP_IFNOT_TIMEOUT(timer, offset)**: Same as previous instruction, but check if the timer has not expired.
- **PSQ_SET_RECOVERY_POINT(address)**: The power ON sequence will start at the address step after a soft reset.
- **PSQ_END**: End of power sequence
- **PSQ_FAIL**: Terminate the power sequence due to an error

### Sensors

This node specifies the sensors monitored by the CERN-IPMC. Only sensor with already existing drivers and dedicated configuration file can be
implemented using the XML configuration file. If the sensor is not supported, please send a mail to the CERN xTCA evaluation project team.

Supported sensor(s):
-   MCP9801
-   AD7417

Foreseen:
-   DS75
-   ADT7462

```xml
<SensorList>
	
	<Sensors type="MCP9801">                                <!-- List of the MCP9801 sensors monitored by the IPNC -->
		
		<Sensor>                                            <!-- Sensor description -->
			<Name>Internal temp.</Name>                     <!-- Sensor name -->
			<NominalReading>25</NominalReading>             <!-- Nominal reading -->
			<NormalMaximum>60</NormalMaximum>               <!-- Normal maximum -->
			<NormalMinimum>-10</NormalMinimum>              <!-- Normal minimum -->
			
			<Type>Temperature</Type>                        <!-- Define the sensor type (Sensor driver document [1.1 Sensor types and units]) -->
			<Units>degrees C</Units>                        <!-- Define the measurement data unit (Sensor driver document [1.1 Sensor types and units]) -->
			
			<Point id="0" x="0" y="0" />                    <!-- 2 points are required to calculate the -->
			<Point id="1" x="5" y="5" />                    <!-- raw to decimal formula -->
			
			<i2c_addr>0x090</i2c_addr>                      <!-- I2C address of the sensor -->
			
			<Thresholds>                                    <!-- Thesholds -->
				<UpperNonRecovery>80</UpperNonRecovery>     <!-- Upper non recovery -->
				<UpperCritical>60</UpperCritical>           <!-- Upper critical -->
				<UpperNonCritical>40</UpperNonCritical>     <!-- Upper non critical -->
				<LowerNonRecovery>-20</LowerNonRecovery>    <!-- Lower non recovery -->
				<LowerCritical>-10</LowerCritical>          <!-- Lower critical -->
				<LowerNonCritical>0</LowerNonCritical>      <!-- Lower non critical -->
			</Thresholds>                                   <!-- End of thresholds list -->
			
		</Sensor>                                           <!-- End of sensor description -->
		
		<!-- Can contains additional MCP9801 sensors -->
		
	</Sensors>
	
	<!-- Can contains additional sensor types -->
		
</SensorList>
```

# Foreseen

-	Adding configuration file options: e-keying
