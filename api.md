I'll try to compile some information about the use of the IPMC project. IPMC sersor examples are [here](https://gitlab.cern.ch/ep-ese-be-xtca/ipmc-project/).

# Low level functions

From `sensor driver` document:

    The I2C based sensors can be controlled using the following functions:
    
    char i2c_dev_write_reg(unsigned short addr,
                           unsigned char reg,
                           unsigned char *pdata,
                           unsigned char size);
                           
    char i2c_dev_read_reg(unsigned short addr,
                          unsigned char reg,
                          unsigned char *pdata,
                          unsigned char size)

    char i2c_dev_read(unsigned short addr,
                      unsigned char *pdata,
                      unsigned char size)

    char i2c_dev_write_2bytesReg(unsigned short addr,
                                 unsigned short reg, 
                                 unsigned char *pdata,
                                 unsigned char size)

    char i2c_dev_read_2bytesReg(unsigned short addr,
                                unsigned short reg,
                                unsigned char *pdata,
                                unsigned char size)


# GPIO

## Functions found in examples (no other documentation)

Functions below are used in C code only

    signal_activate

    signal_deactivate

    signal_set_pin

    signal_read

## initialization

According to email exchanges with Julian, all GPIO signals are input until otherwise configured, which is usually done in the ipmc-user folder.