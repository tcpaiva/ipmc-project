/***********************************************************************

Nom ......... : user_tcpserv.c
Role ........ : TCP/IP server example
Auteur ...... : Julian Mendez <julian.mendez@cern.ch>
Version ..... : V1.0 - 25/10/2017

Compilation :
	- Add the file into the FILES list located in ipmc-user/nr-mkfrag

***********************************************************************/

#include <app.h>
#include <cfgint.h>
#include <ipmc.h>
#include <log.h>
#include <debug.h>

#include <net/tcp.h>
#include <net/ip.h>

#include <user_tcpserv.h>

/* ================================================================ */

/* Connection slot array used to manage the client */
user_tcpserv_users_t user_tcpserv_clients[MAX_USER_TCPSERV_CLIENT];

/* INIT_CALLBACK: called at initialisation of the IPMC */
INIT_CALLBACK(user_tcpserv_init)
{
	/* Connect TCP server using the following parameters:
		- user_tcpserv_connect_handler: callback function called when a client sends a connection request
		- user_tcpserv_disconnect_handler: callback function called when a client is disconnected
		- user_tcpserv_data_handler: callback called when data are received
		- 2004: port number used
	*/
    tcp_connect_server(user_tcpserv_connect_handler, user_tcpserv_disconnect_handler, user_tcpserv_data_handler, 23);
}

/* TIMER_CALLBACK: called every 1s */
TIMER_CALLBACK(1s, user_tcpserv_timercback)
{
	unsigned i;

	unsigned char *data = (unsigned char *)"USER_TCPSERV_TIMERCBACK \n\r";
	unsigned len = 26;
	
	/* Scan the connection slot to send data to all of the clients */
    for(i=0; i < MAX_USER_TCPSERV_CLIENT; i++){
		
		/* Check if the slot is in use */
		if(user_tcpserv_clients[i].opened){
			
			/* Send "len" bytes of "data" */
			tcp_send_packet(user_tcpserv_clients[i].to, user_tcpserv_clients[i].to_port, data, len);
			
		}
		
	}
}

/* 
 * Name: user_tcpserv_connect_handler
 *
 * Parameters:
 *		- from: client IP address
 *		- from_port: client tcp port
 *
 * Description: Called when a client requests a connection. The prototype of this function is defined by the TCP/IP library
 *              and cannot be changed.
 */
char user_tcpserv_connect_handler(const ip_addr_t from, unsigned short from_port){
	unsigned i;

	/* Display information in debug console */
	debug_printf("<_> user_tcpserv: " "New connection request\n");

	/* Save client information in a list */
	for(i=0; i < MAX_USER_TCPSERV_CLIENT; i++){							/* Scan the array to find an empty slot */
		if(user_tcpserv_clients[i].opened == 0){							/* Check if current position is already used */
			user_tcpserv_clients[i].to_port = from_port;					/* */
			memcpy(user_tcpserv_clients[i].to, from, sizeof(from));			/* If not, save the client info */
			user_tcpserv_clients[i].opened = 1;								/* */

			return 0;														/* Return successfully */
		}
	}

	/* Print a warning message when no slot is available */
	debug_printf("<W> user_tcpserv: " "No user_tcpserv connection slot available\n");
	
	/* And quit de function with error */
	return 1;
}

/* 
 * Name: user_tcpserv_disconnect_handler
 *
 * Parameters:
 *		- from: client IP address
 *		- from_port: client tcp port
 *
 * Description: Called when a client sends a disconnect request. The prototype of this function is defined by the TCP/IP library
 *              and cannot be changed.
 */
char user_tcpserv_disconnect_handler(const ip_addr_t from, unsigned short from_port){
	unsigned i;

	/* Display information in debug console */
	debug_printf("<_> user_tcpserv: " "Disconnect request received\n");

	/* Search for the client slot in the array */
	for(i=0; i < MAX_USER_TCPSERV_CLIENT; i++){
		
		/* Check the information */
		if(user_tcpserv_clients[i].opened == 1 && !memcmp(from, user_tcpserv_clients[i].to, sizeof(from)) && from_port == user_tcpserv_clients[i].to_port){
			
			/* Remove the information and set the opened variable to 0 (slot not used) */
			user_tcpserv_clients[i].to_port = 0;
			memset(user_tcpserv_clients[i].to, 0, 4);
			user_tcpserv_clients[i].opened = 0;
			
		}
	}

	/* Return successfully */
	return 0;
}

/* 
 * Name: user_tcpserv_data_handler
 *
 * Parameters:
 *		- to: server IP address (IPMC)
 *		- to_port: server tcp port (IPMC)
 *		- from: client IP address
 * 		- from_port: client tcp port
 *		- data: data received(Array)
 *		- len: Length in byte of the data array
 * 		- reply: array to push a reply
 *		- replyLen: Length of the reply in byte
 *
 * Description: Called when a client sends data. The prototype of this function is defined by the TCP/IP library
 *              and cannot be changed.
 */
char user_tcpserv_data_handler(const ip_addr_t to, unsigned short to_port,const ip_addr_t from, unsigned short from_port, const unsigned char *data, unsigned short len, unsigned char *reply, unsigned *replyLen){

	/* Clone the request in the reply buffer */
	memcpy(reply, data, len);
	
	/* Set the reply length */
	*replyLen = len;

	/* Return the function successfully */
	return 0;
}
