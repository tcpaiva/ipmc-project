/***********************************************************************

Nom ......... : user_mainfile.c
Role ........ : Example of user function definition
Auteur ...... : Julian Mendez <julian.mendez@cern.ch>
Version ..... : V1.0 - 25/10/2017

Compilation :
	- Add the file into the FILES list located in ipmc-user/nr-mkfrag

***********************************************************************/
#include <defs.h>
#include <cfgint.h>
#include <app.h>

/* Required to control I/Os */
#include <app/signal.h>

/* INIT_CALLBACK(fctname) is called during the IPMC initialisation */
INIT_CALLBACK(usermain_init)
{
	/* Set signal information in a variable */
	signal_t userio_sig = USER_IO_0; /* can be USER_IO_0 to USER_IO_34 or IPM_IO_0 to IPM_IO_15 */
	
	/* Set signal to output and high level */
	signal_activate(&userio_sig);
		
	/* Set signal to output and high level */
	signal_deactivate(&userio_sig);
	
	/* Set signal in input mode (default mode) */
	signal_set_pin(&userio_sig, SIGNAL_HIGHZ);
	
	/* Read the signal value (returned value)*/
	int x = signal_read(&userio_sig);
}

unsigned char io_sate_mainloop = 0;

/* MAIN_LOOP_CALLBACK(fctname) is called at every execution of the main loop */
MAIN_LOOP_CALLBACK(usermain_mainloop)
{
	/* Toggle the USER_IO_20 pin */
	signal_t userio_sig = USER_IO_20; 
	
	if (io_sate_mainloop){
		signal_deactivate(&userio_sig);
		io_sate_mainloop = 0;
	} else{
		signal_activate(&userio_sig);
		io_sate_mainloop = 0xFF;
	}
}


unsigned char io_sate_timercback = 0;

/* TIMER_CALLBACK(time, fctname) is called everytime the timer exceed the time value */
TIMER_CALLBACK(1s, usermain_timercback)
{
	/* Toggle the USER_IO_20 pin */
	signal_t userio_sig = USER_IO_20; 

	//debug_printf("User callback \n\r");

	if (io_sate_timercback){
		signal_deactivate(&userio_sig);
		io_sate_timercback = 0;
	} else{
		signal_activate(&userio_sig);
		io_sate_timercback = 0xFF;
	}
}
