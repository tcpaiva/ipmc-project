/*
	Copyright (c) 2005-2012 Pigeon Point Systems.
    All rights reserved.
    Pigeon Point Systems proprietary and confidential.

	Description:
	This header file defines the interfaces
	provided by the sensor_PIM400.c module.
    
    $Revision: 12269 $
*/

#ifndef __MASTER_SENSOR_PIM400_H__
#define __MASTER_SENSOR_PIM400_H__

#include <sensor.h>

/* Read-only info structure of an PIM400 temperature sensor */
typedef struct {
    sensor_ro_t s;

    unsigned char chan; /* Sensor channel number */
} sensor_pim400_ro_t;

/* PIM400 temperature sensor methods */
extern sensor_methods_t PROGMEM sensor_pim400_methods;

/* Auxiliary macro for defining PIM400 sensor info */
#define SENSOR_PIM400(s, c, alert) \
    { \
	SA(sensor_pim400_methods, s, alert), \
	chan: (c) \
    }

#endif /* __MASTER_SENSOR_PIM400_H__ */
