/*
    Copyright (c) 2005-2012 Pigeon Point Systems.
    All rights reserved.
    Pigeon Point Systems proprietary and confidential.

    Description:
	This module implements the AD7417 sensors.

    $Revision: 12269 $
*/

#define NEED_MASTERONLY_I2C

#include <defs.h>
#include <cfgint.h>

#ifdef CFG_SENSOR_AD7417

#include <hal/i2c.h>
#include <hal/system.h>

#include <app.h>
#include <log.h>
#include <sensor.h>
#include <sensor_discrete.h>
#include <sensor_threshold.h>
#include <sensor_ad7417.h>
#include <i2c_dev.h>

#ifndef HAS_MASTERONLY_I2C
#error Enable master-only I2C support to use AD7417 sensors.
#endif

#undef DEBUG

/* ------------------------------------------------------------------ */

/* AD7417 Register definitions */
#define AD7417_TEMP_REG		0
#define AD7417_CFG_REG		1
#define AD7417_THYST_REG	2
#define AD7417_TOTI_REG		3
#define AD7417_ADC_REG		4
#define AD7417_CFG2_REG		5

#define AD7417_CFG_SD		(1 << 0)	/* shutdown bit */
#define AD7417_CFG_POL		(1 << 2)	/* OTI polarity bit */
#define AD7417_CFG_CHAN(x)	((x)<<5)	/* channel selection */

/* Fault Queue bits */
#define AD7417_CFG_FT_1		(0 << 3)	/* 1 fault */
#define AD7417_CFG_FT_2		(1 << 3)	/* 2 faults */
#define AD7417_CFG_FT_4		(2 << 3)	/* 4 faults */
#define AD7417_CFG_FT_6		(3 << 3)	/* 6 faults */

/* ------------------------------------------------------------------ */

/* Forward declarations */
static char sensor_ad7417_init(sensor_t *sensor);
static char sensor_ad7417_fill_reading(sensor_t *sensor,
	unsigned char *msg);

/* ------------------------------------------------------------------ */

/* AD7417 temperature sensor methods */
sensor_methods_t PROGMEM sensor_ad7417_methods = {
    fill_event:		&sensor_threshold_fill_event,
    fill_reading:	&sensor_ad7417_fill_reading,
    rearm:		&sensor_threshold_rearm,
    set_thresholds:	&sensor_threshold_set_thresholds,
    get_thresholds:	&sensor_threshold_get_thresholds,
    set_hysteresis:	&sensor_threshold_set_hysteresis,
    get_hysteresis:	&sensor_threshold_get_hysteresis,
    init:		&sensor_ad7417_init
};

/* Read-only info structures of AD7417 temperature sensors */
static const sensor_ad7417_ro_t PROGMEM sensor_ad7417_ro[] =
	{ CFG_SENSOR_AD7417 };

#define AD7417_COUNT	sizeofarray(sensor_ad7417_ro)

/* Read-write info structures of AD7417 temperature sensors */
static struct sensor_ad7417 {
    sensor_threshold_t	sensor;
} sensor_ad7417[AD7417_COUNT] WARM_BSS;
typedef struct sensor_ad7417 sensor_ad7417_t;

static unsigned short sensor_ad7417_first;
DECL_SENSOR_GROUP(master, sensor_ad7417_ro, sensor_ad7417,
	&sensor_ad7417_first);

/* Global flags for all AD7417 sensors */
static unsigned char sensor_ad7417_global_flags;
#define AD7417_GLOBAL_INIT	(1 << 0)	/* initialize all sensors */
#define AD7417_GLOBAL_UPDATE	(1 << 1)	/* update all sensors */

/* ------------------------------------------------------------------ */

/*
    Internal functions for accessing AD7417
*/

/* Write the AD7417 configuration register */
static char inline sensor_ad7417_write_conf(unsigned short addr,
	unsigned char conf)
{
    return i2c_dev_write_reg(addr, AD7417_CFG_REG, &conf, 1);
}

/* Write the AD7417 configuration2 register */
static char inline sensor_ad7417_write_conf2(unsigned short addr,
	unsigned char conf)
{
    return i2c_dev_write_reg(addr, AD7417_CFG2_REG, &conf, 1);
}

/* Write an AD7417 temperature/adc register (temp, Tos, Thyst or ADC) */
static char inline sensor_ad7417_write_temp_adc(unsigned short addr,
	unsigned char reg, char temp)
{
    unsigned char data[2] = { temp, 0 };

    return i2c_dev_write_reg(addr, reg, data, 2);
}

/* Read an AD7417 temperature/adc register (temp, Tos, Thyst or ADC) */
static char inline sensor_ad7417_read_temp_adc(unsigned short addr,
	unsigned char reg, unsigned char *temp)
{
    unsigned char data[2];

    if (i2c_dev_read_reg(addr, reg, data, 2)) {
	return -1;
    }

    *temp = data[0];

#ifdef DEBUG
    debug_printf("ad7417 @ %02x, temp = %02x \n", addr, data[0]);
#endif

    return 0;
}

/* ------------------------------------------------------------------ */

/*
    The following function updates the reading
    of the given AD7417 sensor.
*/
static void sensor_ad7417_update_reading(unsigned char num, unsigned char flags)
{
    unsigned char snum, reg, chan, reading = 0;
    unsigned short addr;

    /* get the I2C address of AD7417, and channel number */
    snum = sensor_ad7417_first + num;
    addr = PRG_RD(sensor_ad7417_ro[num].i2c_addr);
    chan = PRG_RD(sensor_ad7417_ro[num].channel);

    if (monly_i2c_is_ready(addr)) {
	/* select channel */
	sensor_ad7417_write_conf(addr, AD7417_CFG_CHAN(chan));
	/* read temperature or adc */
	reg = chan ? AD7417_ADC_REG : AD7417_TEMP_REG;
	if (!sensor_ad7417_read_temp_adc(addr, reg, &reading)) {
	    /* update sensor reading */
	    sensor_threshold_update(&master_sensor_set, snum, reading, flags);
	}
    }
}

/*
    The following function initializes the given
    AD7417 sensor.
*/
static void sensor_ad7417_initialize(unsigned char num)
{
    unsigned short addr;

    /* get I2C address of AD7417 */
    addr = PRG_RD(sensor_ad7417_ro[num].i2c_addr);

#ifdef DEBUG
    debug_printf("ad7417 #%02x, initialize \n", num);
#endif

    if (monly_i2c_is_ready(addr)) {
	/* configure AD7417 */
	sensor_ad7417_write_conf(addr, 0);
	sensor_ad7417_write_conf2(addr, 0);

	/* read the current temperature/adc */
	sensor_ad7417_update_reading(num, SENSOR_INITIAL_UPDATE);
    }
}

/*
    Sensor initialization.
*/
static char sensor_ad7417_init(sensor_t *sensor)
{
    unsigned char num = ((struct sensor_ad7417 *) sensor) - sensor_ad7417;
    sensor_ad7417_initialize(num);
    return 0;
}

/* ------------------------------------------------------------------ */

#ifdef NEED_SLAVE_CALLBACKS
SLAVE_UP_CALLBACK(sensor_ad7417_slave_up)
{
    /* schedule global AD7417 init/update */
    sensor_ad7417_global_flags = AD7417_GLOBAL_INIT | AD7417_GLOBAL_UPDATE;
}

SLAVE_DOWN_CALLBACK(sensor_ad7417_slave_down)
{
    /* unschedule global AD7417 init/update */
    sensor_ad7417_global_flags = 0;
}
#endif

#ifdef NEED_CARRIER_CALLBACKS
CARRIER_UP_CALLBACK(sensor_ad7417_carrier_up)
{
    /* schedule global AD7417 init/update */
    sensor_ad7417_global_flags = AD7417_GLOBAL_INIT | AD7417_GLOBAL_UPDATE;
}

CARRIER_DOWN_CALLBACK(sensor_ad7417_carrier_down)
{
    /* unschedule global AD7417 init/update */
    sensor_ad7417_global_flags = 0;
}
#endif

/* ------------------------------------------------------------------ */

/*
    This section contains various callbacks
    registered by the AD7417 driver.
*/

/* 1 second callback */
TIMER_CALLBACK(1s, sensor_ad7417_1s_callback)
{
    unsigned char flags;

    /* schedule global AD7417 update */
    save_flags_cli(flags);
    sensor_ad7417_global_flags |= AD7417_GLOBAL_UPDATE;
    restore_flags(flags);
}

/* Main loop callback */
MAIN_LOOP_CALLBACK(sensor_ad7417_poll)
{
    unsigned char i, flags, gflags;

    /* get/clear global AD7417 flags */
    save_flags_cli(flags);
    gflags = sensor_ad7417_global_flags;
    sensor_ad7417_global_flags = 0;
    restore_flags(flags);

    if (gflags & AD7417_GLOBAL_INIT) {
	/* make a delay to let the slave/carrier AVRs stabilize */
	udelay(20000);

        /* initialize all AD7417 */
	for (i = 0; i < AD7417_COUNT; i++) {
	    if (!(sensor_ad7417[i].sensor.s.status & STATUS_NOT_PRESENT)) {
		sensor_ad7417_initialize(i);
	    }
	}
    }

    if (gflags & AD7417_GLOBAL_UPDATE) {
	/* update all sensor readings */
	for (i = 0; i < AD7417_COUNT; i++) {
	    if (!(sensor_ad7417[i].sensor.s.status & STATUS_NOT_PRESENT)) {
		sensor_ad7417_update_reading(i, 0);
	    }
	}
    }
}

/* Initialization callback */
INIT_CALLBACK(sensor_ad7417_init_all)
{
    /* schedule global initialization */
    sensor_ad7417_global_flags = AD7417_GLOBAL_INIT | AD7417_GLOBAL_UPDATE;
}

/* ------------------------------------------------------------------ */

/*
    This section contains AD7417 sensor methods.
*/

/* Fill the Get Sensor Reading reply */
static char sensor_ad7417_fill_reading(sensor_t *sensor, unsigned char *msg)
{
    /* update current reading */
    sensor_ad7417_update_reading((sensor_ad7417_t *)sensor - sensor_ad7417, 0);

    /* fill the reply */
    return sensor_threshold_fill_reading(sensor, msg);
}

#endif /* CFG_SENSOR_AD7417 */
