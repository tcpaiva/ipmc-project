/*
    Copyright (c) 2003-2012 Pigeon Point Systems.
    All rights reserved.
    Pigeon Point Systems proprietary and confidential.

    Description:
	This header file defines the interfaces
	provided by the sensor_ds75.c module.
    
    $Revision: 12269 $
*/

#ifndef __MASTER_SENSOR_DS75_H__
#define __MASTER_SENSOR_DS75_H__

#include <sensor.h>

/* Read-only info structure of a DS75 temperature sensor */
typedef struct {
    sensor_ro_t s;

    unsigned short	i2c_addr;	/* I2C address of the chip */
    unsigned char	irq;		/* IRQ line (0xff = no IRQ) */
} sensor_ds75_ro_t;

/* DS75 temperature sensor methods */
extern sensor_methods_t PROGMEM sensor_ds75_methods;

/* Auxiliary macro for defining DS75 sensor info */
#define SENSOR_DS75(s, a, i, alert)		\
    {						\
	SA(sensor_ds75_methods, s, alert),	\
	i2c_addr: (a),				\
	irq: (i)				\
    }

#endif /* __MASTER_SENSOR_DS75_H__ */
