/*
    Copyright (c) 2005-2012 Pigeon Point Systems.
    All rights reserved.
    Pigeon Point Systems proprietary and confidential.

    Description:
	This header file defines the interfaces
	provided by the sensor_ad7417.c module.
    
    $Revision: 12269 $
*/

#ifndef __MASTER_SENSOR_AD7417_H__
#define __MASTER_SENSOR_AD7417_H__

#include <sensor.h>

/* Read-only info structure of an AD7417 temperature sensor */
typedef struct {
    sensor_ro_t s;

    unsigned short	i2c_addr;	/* I2C address of the chip */
    unsigned char	channel;	/* temp/adc channel */
} sensor_ad7417_ro_t;

/* AD7417 temperature sensor methods */
extern sensor_methods_t PROGMEM sensor_ad7417_methods;

/* Auxiliary macro for defining AD7417 sensor info */
#define SENSOR_AD7417(s, a, c, alert)		\
    {						\
	SA(sensor_ad7417_methods, s, alert),	\
	i2c_addr: (a),				\
	channel: (c)				\
    }

#endif /* __MASTER_SENSOR_AD7417_H__ */
