/*
    Copyright (c) 2005-2012 Pigeon Point Systems.
    All rights reserved.
    Pigeon Point Systems proprietary and confidential.

    Description:
	This header file defines the interfaces
	provided by the sensor_adt7462.c module.
    
    $Revision: 12269 $
*/

#ifndef __MASTER_SENSOR_ADT7462_H__
#define __MASTER_SENSOR_ADT7462_H__

#include <sensor.h>

/* Read-only info structure of an ADT7462 temperature sensor */
typedef struct {
    sensor_ro_t s;

    unsigned short	i2c_addr;	/* I2C address of the chip */
    unsigned char	tach;		/* tachometer number */
} sensor_adt7462_ro_t;

/* ADT7462 temperature sensor methods */
extern sensor_methods_t PROGMEM sensor_adt7462_methods;

/* Auxiliary macro for defining ADT7462 sensor info */
#define SENSOR_ADT7462(s, a, c, alert)		\
    {						\
	SA(sensor_adt7462_methods, s, alert),	\
	i2c_addr: (a),				\
	tach: (c)				\
    }

#endif /* __MASTER_SENSOR_ADT7462_H__ */
