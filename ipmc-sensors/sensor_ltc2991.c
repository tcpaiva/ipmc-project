/*
    Copyright (c) 2005-2012 Pigeon Point Systems.
    All rights reserved.
    Pigeon Point Systems proprietary and confidential.

    Description:
	This module implements the LTC2991 sensors.

    $Revision: 12269 $
*/

#define NEED_MASTERONLY_I2C

#include <defs.h>
#include <cfgint.h>

#ifdef CFG_SENSOR_LTC2991

#include <hal/i2c.h>
#include <hal/system.h>

#include <app.h>
#include <log.h>
#include <sensor.h>
#include <sensor_discrete.h>
#include <sensor_threshold.h>
#include <sensor_ltc2991.h>
#include <i2c_dev.h>
#include <sensor_pca9545.h>

#ifndef HAS_MASTERONLY_I2C
#error Enable master-only I2C support to use LTC2991 sensors.
#endif

#undef DEBUG
//#define DEBUG

/* ------------------------------------------------------------------ */

// #define SNS_I2C_BUS      2
// #define I2CMUX_ADDR      0xE0 /* Address of the PCA9545A I2C multiplexer */
#define LTC2991_I2C_ADDR    MO_CHANNEL_ADDRESS(SENSOR_I2C_BUS, 0x4E << 1) /* I2C address of the LTC2991 voltage/temperature monitoring device */
#define LTC2991_SLAVE_SEL   8

/* LTC2991 Register definitions */
#define LTC2991_STATUS_REG      0x00
#define LTC2991_CHAN_ENA_REG    0x01
#define LTC2991_CTRL_V1234_REG  0x06
#define LTC2991_CTRL_V5678_REG  0x07
#define LTC2991_CTRL_TINT_REG   0x08
// #define LTC2991_TRIGGER_REG		0x02
#define LTC2991_ADC_V1_REG		0x0A
#define LTC2991_ADC_V2_REG		0x0C
#define LTC2991_ADC_V3_REG		0x0E
#define LTC2991_ADC_V4_REG		0x10
#define LTC2991_ADC_V5_REG		0x12
#define LTC2991_ADC_V6_REG		0x14
#define LTC2991_ADC_V7_REG		0x16
#define LTC2991_ADC_V8_REG		0x18
#define LTC2991_TINT_REG	    0x1A
#define LTC2991_VCC_REG		    0x1C

#define LTC2991_ENABLE_CHAN     0x78 // enable all channels, except 7 and 8 (unconnected)
#define LTC2991_REPEAT_ACQ      0x10 // eanble rpeated acquisition

/* ------------------------------------------------------------------ */

/* Forward declarations */
static char sensor_ltc2991_init(sensor_t *sensor);
static char sensor_ltc2991_fill_reading(sensor_t *sensor, unsigned char *msg);

/* ------------------------------------------------------------------ */

/* LTC2991 temperature sensor methods */
sensor_methods_t PROGMEM sensor_ltc2991_methods = {
    fill_event:		&sensor_threshold_fill_event,
    fill_reading:	&sensor_ltc2991_fill_reading,
    rearm:			&sensor_threshold_rearm,
    set_thresholds:	&sensor_threshold_set_thresholds,
    get_thresholds:	&sensor_threshold_get_thresholds,
    set_hysteresis:	&sensor_threshold_set_hysteresis,
    get_hysteresis:	&sensor_threshold_get_hysteresis,
    init:			&sensor_ltc2991_init
};

/* Read-only info structures of LTC2991 temperature sensors */
static const sensor_ltc2991_ro_t PROGMEM sensor_ltc2991_ro[] = { CFG_SENSOR_LTC2991 };

#define LTC2991_COUNT	sizeofarray(sensor_ltc2991_ro)

/* Read-write info structures of LTC2991 temperature sensors */
static struct sensor_ltc2991 {
    sensor_threshold_t	sensor;
} sensor_ltc2991[LTC2991_COUNT] WARM_BSS;
typedef struct sensor_ltc2991 sensor_ltc2991_t;

static unsigned short sensor_ltc2991_first;
DECL_SENSOR_GROUP(master, sensor_ltc2991_ro, sensor_ltc2991, &sensor_ltc2991_first);

/* Global flags for all LTC2991 sensors */
static unsigned char sensor_ltc2991_global_flags;
#define LTC2991_GLOBAL_INIT	    (1 << 0)	/* initialize all sensors */
#define LTC2991_GLOBAL_UPDATE	(1 << 1)	/* update all sensors */

/* ------------------------------------------------------------------ */

/*
    Internal functions for accessing LTC2991
*/

/* Write an LTC2991 register */
static char inline sensor_ltc2991_write_reg(unsigned char reg, unsigned char data)
{
	return i2c_dev_write_reg(LTC2991_I2C_ADDR, reg, &data, 1);
}

/* Read an LTC2991 temperature/adc register (temp, ADC) */
static char inline sensor_ltc2991_read_sensor(unsigned char reg, unsigned char *val)
{
    unsigned char data[2];

	*val = 0xFF; // unrealistic value to mark an error
    if (i2c_dev_read_reg(LTC2991_I2C_ADDR, reg, data, 2)) return I2C_ERROR;
	if (reg == LTC2991_TINT_REG) // internal temperature reading
		// assume Temperature is positive and D[12] is always 0, shift in 4 LSBs, resolution is then 0.5 degC
		*val = (data[0] << 5) | ((data[1] >> 3) & 0x1F);
	else if (reg == LTC2991_ADC_V4_REG || reg == LTC2991_ADC_V5_REG) // voltage reading for channels V4 (2.5V) and V5 (3.3V)
		// Sign bit should always be 0, valid bit should be 1, the resolution is then 19.531 mV, the maximum range is 4.98V
		*val = (data[0] << 2) | ((data[1] >> 6) & 0x3);
	else
		// Sign bit and D[13] should always be 0, valid bit should be 1, the resolution is then 9.766 mV, the maximum range is 2.49V
		*val = (data[0] << 3) | ((data[1] >> 5) & 0x7);
#ifdef DEBUG
    debug_printf("LTC2991 at 0x%03x, register 0x%02x: data = 0x%02x%02x, sensor = %d\n", LTC2991_I2C_ADDR, reg, data[0], data [1], *val);
#endif
    return I2C_OK;
}

/* ------------------------------------------------------------------ */

/*
    The following function updates the reading
    of the given LTC2991 sensor.
*/
static void sensor_ltc2991_update_reading(unsigned char num, unsigned char flags)
{
    unsigned char reg, reading;
    /* get the I2C address of LTC2991, and channel number */
    unsigned char snum = sensor_ltc2991_first + num;
    unsigned char chan = PRG_RD(sensor_ltc2991_ro[num].sns);

    if (monly_i2c_is_ready(LTC2991_I2C_ADDR)) {
		/* select I2C switch slave channel */
		if (sensor_pca9545_write(LTC2991_SLAVE_SEL) != I2C_OK) {
			debug_printf(PSTR("Failed to write to PCA9545A I2C switch\n"));
			return;
		}
		/* read temperature or adc */
		reg = LTC2991_ADC_V1_REG + (2 * chan);
		if (!sensor_ltc2991_read_sensor(reg, &reading)) {
			/* update sensor reading */
			sensor_threshold_update(&master_sensor_set, snum, reading, flags);
		}
		/* disable I2C switch */
		//sensor_ltc2991_write_mux(0);
    }
}

/*
    The following function initializes the given
    LTC2991 sensor.
*/
static void sensor_ltc2991_initialize(unsigned char num)
{
    unsigned char chan = PRG_RD(sensor_ltc2991_ro[num].sns);

#ifdef DEBUG
    debug_printf("ltc2991 #%d, initialize\n", num);
#endif
    if (monly_i2c_is_ready(LTC2991_I2C_ADDR)) {
		if (chan == 0) { // only need to intitialize once per LTC2991 device, not every channel
			/* select I2C switch slave channel */
			if (sensor_pca9545_write(LTC2991_SLAVE_SEL) != I2C_OK) {
				debug_printf(PSTR("Failed to write to PCA9545A I2C switch\n"));
				return;
			}
			/* enable repeated acquisition */
			sensor_ltc2991_write_reg(LTC2991_CTRL_TINT_REG, LTC2991_REPEAT_ACQ);
			/* configure LTC2991 channel enable register, triggers the acquisition */
			sensor_ltc2991_write_reg(LTC2991_CHAN_ENA_REG, LTC2991_ENABLE_CHAN);
			/* disable I2C switch */
			//sensor_ltc2991_write_mux(0);
		}
		/* read the current sensor value */
		sensor_ltc2991_update_reading(num, SENSOR_INITIAL_UPDATE);
    }
}

/*
    Sensor initialization.
*/
static char sensor_ltc2991_init(sensor_t *sensor)
{
    unsigned char num = ((struct sensor_ltc2991 *) sensor) - sensor_ltc2991;
    sensor_ltc2991_initialize(num);
    return 0;
}

/* ------------------------------------------------------------------ */

#ifdef NEED_SLAVE_CALLBACKS
SLAVE_UP_CALLBACK(sensor_ltc2991_slave_up)
{
    /* schedule global LTC2991 init/update */
    sensor_ltc2991_global_flags = LTC2991_GLOBAL_INIT | LTC2991_GLOBAL_UPDATE;
}

SLAVE_DOWN_CALLBACK(sensor_ltc2991_slave_down)
{
    /* unschedule global LTC2991 init/update */
    sensor_ltc2991_global_flags = 0;
}
#endif

#ifdef NEED_CARRIER_CALLBACKS
CARRIER_UP_CALLBACK(sensor_ltc2991_carrier_up)
{
    /* schedule global LTC2991 init/update */
    sensor_ltc2991_global_flags = LTC2991_GLOBAL_INIT | LTC2991_GLOBAL_UPDATE;
}

CARRIER_DOWN_CALLBACK(sensor_ltc2991_carrier_down)
{
    /* unschedule global LTC2991 init/update */
    sensor_ltc2991_global_flags = 0;
}
#endif

/* ------------------------------------------------------------------ */

/*
    This section contains various callbacks
    registered by the LTC2991 driver.
*/

/* 1 second callback */
TIMER_CALLBACK(1s, sensor_ltc2991_1s_callback)
{
    unsigned char flags;

    /* schedule global LTC2991 update */
    save_flags_cli(flags);
    sensor_ltc2991_global_flags |= LTC2991_GLOBAL_UPDATE;
    restore_flags(flags);
}

/* Main loop callback */
MAIN_LOOP_CALLBACK(sensor_ltc2991_poll)
{
    unsigned char i, flags, gflags;

    /* get/clear global LTC2991 flags */
    save_flags_cli(flags);
    gflags = sensor_ltc2991_global_flags;
    sensor_ltc2991_global_flags = 0;
    restore_flags(flags);

    if (gflags & LTC2991_GLOBAL_INIT) {
	/* make a delay to let the slave/carrier AVRs stabilize */
	udelay(20000);

        /* initialize all LTC2991 */
	for (i = 0; i < LTC2991_COUNT; i++) {
	    if (!(sensor_ltc2991[i].sensor.s.status & STATUS_NOT_PRESENT)) {
		sensor_ltc2991_initialize(i);
	    }
	}
    }

    if (gflags & LTC2991_GLOBAL_UPDATE) {
	/* update all sensor readings */
	for (i = 0; i < LTC2991_COUNT; i++) {
	    if (!(sensor_ltc2991[i].sensor.s.status & STATUS_NOT_PRESENT)) {
		sensor_ltc2991_update_reading(i, 0);
	    }
	}
    }
}

/* Initialization callback */
INIT_CALLBACK(sensor_ltc2991_init_all)
{
    /* schedule global initialization */
    sensor_ltc2991_global_flags = LTC2991_GLOBAL_INIT | LTC2991_GLOBAL_UPDATE;
}

/* ------------------------------------------------------------------ */

/*
    This section contains LTC2991 sensor methods.
*/

/* Fill the Get Sensor Reading reply */
static char sensor_ltc2991_fill_reading(sensor_t *sensor, unsigned char *msg)
{
    /* update current reading */
    sensor_ltc2991_update_reading((sensor_ltc2991_t *)sensor - sensor_ltc2991, 0);

    /* fill the reply */
    return sensor_threshold_fill_reading(sensor, msg);
}

#endif /* CFG_SENSOR_LTC2991 */
