/*
    Copyright (c) 2005-2012 Pigeon Point Systems.
    All rights reserved.
    Pigeon Point Systems proprietary and confidential.

    Description:
	This header file defines the interfaces
	provided by the sensor_template.c module.

    $Revision: 12269 $
*/

#ifndef __MASTER_SENSOR_TEMPLATE_H__
#define __MASTER_SENSOR_TEMPLATE_H__

#include <sensor.h>

/* Read-only info structure of an Template sensor */
typedef struct {
    sensor_ro_t s;
    unsigned short i2c_addr;	/* I2C address of the chip */
} sensor_template_ro_t;

/* Template sensor methods */
extern sensor_methods_t PROGMEM sensor_template_methods;

static char sensor_template_fill_rd(sensor_t *sensor, unsigned char *msg);

/* Auxiliary macro for defining Template sensor info */
#define SENSOR_TEMPLATE(s, i2c_addr_p, alert)		\
    {						\
		SA(sensor_template_methods, s, alert),	\
		i2c_addr: (i2c_addr_p) \
    }
    
#endif /* __MASTER_SENSOR_TEMPLATE_H__ */
