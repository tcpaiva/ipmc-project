/*
    Copyright (c) 2005-2012 Pigeon Point Systems.
    All rights reserved.
    Pigeon Point Systems proprietary and confidential.

    Description:
	This module implements the MCP9801 sensors.

    $Revision: 12269 $
*/

#define NEED_MASTERONLY_I2C

#include <defs.h>
#include <cfgint.h>
#include <debug.h>

#ifdef CFG_SENSOR_MCP9801

#include <hal/i2c.h>
#include <hal/system.h>

#include <app.h>
#include <log.h>
#include <sensor.h>
#include <sensor_discrete.h>
#include <sensor_threshold.h>
#include <sensor_mcp9801.h>
#include <i2c_dev.h>

#ifndef HAS_MASTERONLY_I2C
#error Enable master-only I2C support to use MCP9801 sensors.
#endif

/*
#ifndef DEBUG
	#define DEBUG
#endif
*/

/* ------------------------------------------------------------------ */

/* MCP9801 Register definitions */
#define MCP9801_TEMP_REG		0
#define MCP9801_CFG_REG		1
#define MCP9801_THYST_REG	2
#define MCP9801_TOTI_REG		3
#define MCP9801_ADC_REG		4
#define MCP9801_CFG2_REG		5

#define MCP9801_CFG_SD		(1 << 0)	/* shutdown bit */
#define MCP9801_CFG_POL		(1 << 2)	/* OTI polarity bit */
#define MCP9801_CFG_CHAN(x)	((x)<<5)	/* channel selection */

/* Fault Queue bits */
#define MCP9801_CFG_FT_1		(0 << 3)	/* 1 fault */
#define MCP9801_CFG_FT_2		(1 << 3)	/* 2 faults */
#define MCP9801_CFG_FT_4		(2 << 3)	/* 4 faults */
#define MCP9801_CFG_FT_6		(3 << 3)	/* 6 faults */

/* ------------------------------------------------------------------ */

/* Forward declarations */
static char sensor_mcp9801_init(sensor_t *sensor);
static char sensor_mcp9801_fill_reading(sensor_t *sensor, unsigned char *msg);

/* ------------------------------------------------------------------ */

/* MCP9801 temperature sensor methods */
sensor_methods_t PROGMEM sensor_mcp9801_methods = {
    fill_event:		&sensor_threshold_fill_event,
    fill_reading:	&sensor_mcp9801_fill_reading,
    rearm:			&sensor_threshold_rearm,
    set_thresholds:	&sensor_threshold_set_thresholds,
    get_thresholds:	&sensor_threshold_get_thresholds,
    set_hysteresis:	&sensor_threshold_set_hysteresis,
    get_hysteresis:	&sensor_threshold_get_hysteresis,
    init:			&sensor_mcp9801_init
};

/* Read-only info structures of MCP9801 temperature sensors */
static const sensor_mcp9801_ro_t PROGMEM sensor_mcp9801_ro[] =
	{ CFG_SENSOR_MCP9801 };

#define MCP9801_COUNT	sizeofarray(sensor_mcp9801_ro)

/* Read-write info structures of MCP9801 temperature sensors */
static struct sensor_mcp9801 {
    sensor_threshold_t	sensor;
} sensor_mcp9801[MCP9801_COUNT] WARM_BSS;
typedef struct sensor_mcp9801 sensor_mcp9801_t;

static unsigned short sensor_mcp9801_first;
DECL_SENSOR_GROUP(master, sensor_mcp9801_ro, sensor_mcp9801, &sensor_mcp9801_first);

/* Global flags for all MCP9801 sensors */
static unsigned char sensor_mcp9801_global_flags;
#define MCP9801_GLOBAL_INIT		(1 << 0)	/* initialize all sensors */
#define MCP9801_GLOBAL_UPDATE	(1 << 1)	/* update all sensors */

/* ------------------------------------------------------------------ */

/*
    Internal functions for accessing MCP9801
*/


/* ------------------------------------------------------------------ */

/*
    The following function updates the reading
    of the given MCP9801 sensor.
*/
static void sensor_mcp9801_update_reading(unsigned char num, unsigned char flags){
    unsigned char snum;
    unsigned short addr;
	unsigned char data[2];
	unsigned char val;

    /* get the I2C address of MCP9801, and channel number */
    snum = sensor_mcp9801_first + num;
    addr = PRG_RD(sensor_mcp9801_ro[num].i2c_addr);

    if (monly_i2c_is_ready(addr)) {

    	if (!i2c_dev_read_reg(addr, 0x00, data, 2)) {
			/*
			data[0] &= 0x1F;

			if((data[1] & 0x10) == 0x10) 	val = 0;
			else							val = (data[1] << 5) + (data[0] >> 3);
			*/

    		val = data[0];

			#ifdef DEBUG
    		debug_printf("MCP9801 val: %d \n", val);
			#endif

			sensor_threshold_update(&master_sensor_set, snum, val, flags);
		}else{

			#ifdef DEBUG
			debug_printf(PSTR("MCP9801 #Error reading \n"));
			#endif
		}
    }
}

/*
    The following function initializes the given
    MCP9801 sensor.
*/
static void sensor_mcp9801_initialize(unsigned char num)
{
    unsigned short addr;
    unsigned char data[1] = { 0x00 };

    /* get I2C address of MCP9801 */
    addr = PRG_RD(sensor_mcp9801_ro[num].i2c_addr);

#ifdef DEBUG
    debug_printf("MCP9801 #%02x, initialize \n", num);
#endif

    if (monly_i2c_is_ready(addr)) {
		/* configure MCP9801 */
    	i2c_dev_write_reg(addr, 0x01, data, 1);
		#ifdef DEBUG
    	debug_printf(PSTR("Done \n"));
		#endif

		/* read the current temperature/adc */
		sensor_mcp9801_update_reading(num, SENSOR_INITIAL_UPDATE);
    }else{
		#ifdef DEBUG
    	debug_printf(PSTR("Failed \n"));
		#endif
    }
}

/*
    Sensor initialization.
*/
static char sensor_mcp9801_init(sensor_t *sensor)
{
    unsigned char num = ((struct sensor_mcp9801 *) sensor) - sensor_mcp9801;
    sensor_mcp9801_initialize(num);
    return 0;
}

/* ------------------------------------------------------------------ */

#ifdef NEED_SLAVE_CALLBACKS
	SLAVE_UP_CALLBACK(sensor_mcp9801_slave_up)
	{
		/* schedule global MCP9801 init/update */
		sensor_mcp9801_global_flags = MCP9801_GLOBAL_INIT | MCP9801_GLOBAL_UPDATE;
	}

	SLAVE_DOWN_CALLBACK(sensor_mcp9801_slave_down)
	{
		/* unschedule global MCP9801 init/update */
		sensor_mcp9801_global_flags = 0;
	}
#endif

#ifdef NEED_CARRIER_CALLBACKS
	CARRIER_UP_CALLBACK(sensor_mcp9801_carrier_up)
	{
		/* schedule global MCP9801 init/update */
		sensor_MCP9801_global_flags = MCP9801_GLOBAL_INIT | MCP9801_GLOBAL_UPDATE;
	}

	CARRIER_DOWN_CALLBACK(sensor_mcp9801_carrier_down)
	{
		/* unschedule global MCP9801 init/update */
		sensor_mcp9801_global_flags = 0;
	}
#endif

/* ------------------------------------------------------------------ */

/*
    This section contains various callbacks
    registered by the MCP9801 driver.
*/

/* 1 second callback */
TIMER_CALLBACK(1s, sensor_mcp9801_1s_callback)
{
    unsigned char flags;

    /* schedule global MCP9801 update */
    save_flags_cli(flags);
    sensor_mcp9801_global_flags |= MCP9801_GLOBAL_UPDATE;
    restore_flags(flags);
}

/* Main loop callback */
MAIN_LOOP_CALLBACK(sensor_mcp9801_poll)
{
    unsigned char i, flags, gflags;

    /* get/clear global MCP9801 flags */
    save_flags_cli(flags);
    gflags = sensor_mcp9801_global_flags;
    sensor_mcp9801_global_flags = 0;
    restore_flags(flags);

    if (gflags & MCP9801_GLOBAL_INIT) {
		/* make a delay to let the slave/carrier AVRs stabilize */
		udelay(20000);

			/* initialize all MCP9801 */
		for (i = 0; i < MCP9801_COUNT; i++) {
			if (!(sensor_mcp9801[i].sensor.s.status & STATUS_NOT_PRESENT)) {
				sensor_mcp9801_initialize(i);
			}
		}
    }

    if (gflags & MCP9801_GLOBAL_UPDATE) {
		/* update all sensor readings */
		for (i = 0; i < MCP9801_COUNT; i++) {
			if (!(sensor_mcp9801[i].sensor.s.status & STATUS_NOT_PRESENT)) {
				sensor_mcp9801_update_reading(i, 0);
			}
		}
    }
}

/* Initialization callback */
INIT_CALLBACK(sensor_mcp9801_init_all)
{
    /* schedule global initialization */
    sensor_mcp9801_global_flags = MCP9801_GLOBAL_INIT | MCP9801_GLOBAL_UPDATE;
}

/* ------------------------------------------------------------------ */

/*
    This section contains MCP9801 sensor methods.
*/

/* Fill the Get Sensor Reading reply */
static char sensor_mcp9801_fill_reading(sensor_t *sensor, unsigned char *msg)
{
    /* update current reading */
    sensor_mcp9801_update_reading((sensor_mcp9801_t *)sensor - sensor_mcp9801, 0);

    /* fill the reply */
    return sensor_threshold_fill_reading(sensor, msg);
}

#endif /* CFG_SENSOR_MCP9801 */
